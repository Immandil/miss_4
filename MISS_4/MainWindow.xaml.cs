﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MISS_4
{
    using System;
    using OxyPlot;
    using OxyPlot.Series;
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
    }

    public class MainWindowModel
    {
        public PlotModel MyModel { get; private set; }
        private double precision;
        private double x0;

        public MainWindowModel()
        {
            string[] args = Environment.GetCommandLineArgs();
            if (args.Length <= 3)
            {
                x0 = Convert.ToDouble(args[1]);
                precision = Convert.ToDouble(args[2]);
                current = x0;
                this.MyModel = new PlotModel { Title = "Graph" };
                this.MyModel.Series.Add(new FunctionSeries(EulerCauchy, 0, 1.0, precision, "EulerCauchy"));
                //this.MyModel.Series.Add(new FunctionSeries(CauchyProblem, 0, 1.0, precision, "CauchyProblem"));
            }
        }

        private double f(double x, double t)
        {
            //return (t / 2.0 * x) + (x * t) / (2.0 * (t * t - 1));
            return x + t;
        }

        private double current;

        // Source: http://www.imio.polsl.pl/dopobrania/dmrrrz.pdf
        private double EulerCauchy(double t)
        {
            double result = current;
            double yG = current + precision * f(current, t);
            double m = f(yG, t + precision);
            current = current + 0.5 * precision * (f(current, t) + m);
            return result;
        }
    }
}
